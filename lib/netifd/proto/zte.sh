#!/bin/sh

. /lib/functions.sh
. ../netifd-proto.sh
init_proto "$@"

proto_zte_init_config() {
	renew_handler=1

	proto_config_add_string username
	proto_config_add_string password
	proto_config_add_string 'ipaddr:ipaddr'
	proto_config_add_string 'hostname:hostname'
	proto_config_add_string clientid
	proto_config_add_string vendorid
	proto_config_add_boolean 'broadcast:bool'
	proto_config_add_string 'reqopts:list(string)'
	proto_config_add_string iface6rd
	proto_config_add_string sendopts
	proto_config_add_boolean delegate
	proto_config_add_string zone6rd
	proto_config_add_string zone
	proto_config_add_string mtu6rd
	proto_config_add_string customroutes
}

proto_zte_setup() {
	local config="$1"
	local iface="$2"
	local wpa_config=/var/run/wpa_supplicant.conf
	local retry=10

	local ipaddr hostname clientid vendorid broadcast reqopts iface6rd sendopts delegate zone6rd zone mtu6rd customroutes
	json_get_vars ipaddr hostname clientid vendorid broadcast reqopts iface6rd sendopts delegate zone6rd zone mtu6rd customroutes

	local opt dhcpopts
	for opt in $reqopts; do
		append dhcpopts "-O $opt"
	done

	for opt in $sendopts; do
		append dhcpopts "-x $opt"
	done

	[ "$broadcast" = 1 ] && broadcast="-B" || broadcast=
	[ -n "$clientid" ] && clientid="-x 0x3d:${clientid//:/}" || clientid="-C"
	[ -n "$iface6rd" ] && proto_export "IFACE6RD=$iface6rd"
	[ "$iface6rd" != 0 -a -f /lib/netifd/proto/6rd.sh ] && append dhcpopts "-O 212"
	[ -n "$zone6rd" ] && proto_export "ZONE6RD=$zone6rd"
	[ -n "$zone" ] && proto_export "ZONE=$zone"
	[ -n "$mtu6rd" ] && proto_export "MTU6RD=$mtu6rd"
	[ -n "$customroutes" ] && proto_export "CUSTOMROUTES=$customroutes"
	[ "$delegate" = "0" ] && proto_export "IFACE6RD_DELEGATE=0"

	proto_export "INTERFACE=$config"

	# wpa config
	json_get_var username username
	json_get_var password password
	echo ctrl_interface=DIR=/var/run/wpa_supplicant > $wpa_config
	echo ap_scan=0 >> $wpa_config
	echo network={ >> $wpa_config
	echo key_mgmt=IEEE8021X >> $wpa_config
	echo eap=PEAP >> $wpa_config
	echo phase1=\"peaplabel=1 tls_disable_time_checks=1\" >> $wpa_config
	echo identity=\"$username\" >> $wpa_config
	echo password=\"$password\" >> $wpa_config
	echo phase2=\"auth=GTC\" >> $wpa_config
	echo } >> $wpa_config

	wpa_supplicant \
		-B \
		-D wired \
		-i $iface \
		-c $wpa_config
	proto_run_command "$config" udhcpc \
		-p /var/run/udhcpc-$iface.pid \
		-s /lib/netifd/dhcp.script \
		-f -t 0 -i "$iface" \
		${ipaddr:+-r $ipaddr} \
		${hostname:+-H $hostname} \
		${vendorid:+-V $vendorid} \
		$clientid $broadcast $dhcpopts

	wpa_cli interface $iface
	wpa_cli logoff
	wpa_cli logon
	until [[ $retry > 0 && `wpa_cli status | grep suppPortStatus=Authorize` ]]
	do
		let retry=retry-1
		sleep 1
	done
	proto_zte_renew $config
}

proto_zte_renew() {
	local interface="$1"
	# SIGUSR1 forces udhcpc to renew its lease
	local sigusr1="$(kill -l SIGUSR1)"
	[ -n "$sigusr1" ] && proto_kill_command "$interface" $sigusr1
}

proto_zte_teardown() {
	local interface="$1"
	proto_kill_command "$interface"
}

add_protocol zte
