-- Copyright 2016 Xu Rong <xurongs@hotmail.com>
-- Licensed to the public under the Apache License 2.0.

local proto = luci.model.network:register_protocol("zte")

function proto.get_i18n(self)
	return luci.i18n.translate("ZTE UDS")
end

function proto.is_installed(self)
	return nixio.fs.access("/lib/netifd/proto/zte.sh")
end

function proto.opkg_package(self)
	return "zte-uds"
end
